'use strict';

angular.module('conceptFocusToolApp')


.controller('ConceptListController', ['$scope', '$state', '$orderProvider', 'conceptListFactory', function($scope, $state, $orderProvider, conceptListFactory) {

    // populate an array with each and every of the user's concepts
    $scope.cards = [];
    $scope.orderParam = $orderProvider.getOrderParam();
    $scope.isReversed = $orderProvider.getIsReversed();

    conceptListFactory.query(
        function (response) {
            $scope.cards = response;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        }
    );

    $scope.putInLexicographicOrder = function() {
        $scope.orderParam = "name";
        $orderProvider.setOrderParam($scope.orderParam);
    };

    $scope.putInWhenAddedOrder = function() {
        $scope.orderParam = "createdAt";
        $orderProvider.setOrderParam($scope.orderParam);
    };

    $scope.putInWhenModifiedOrder = function() {
        $scope.orderParam = "updatedAt";
        $orderProvider.setOrderParam($scope.orderParam);
    };

    $scope.reverseOrder = function() {
        $scope.isReversed = !$scope.isReversed;
        $orderProvider.setIsReversed($scope.isReversed);
    };

    $scope.createRequested = function() {
        $state.go('app.conceptsitemnew');
    };

    $scope.editConcept = function(cardId) {
        $state.go('app.conceptsitemedit', { id:cardId });
    };
}])


.controller('ConceptsItemViewCtrl', ['$scope', '$state', '$stateParams', 'cardFactory', function($scope, $state, $stateParams, cardFactory) {

    // get user's data for this item
    $scope.cardId = $stateParams.id;
    $scope.card = {};

    cardFactory.get({ id: $scope.cardId })
    .$promise.then(
        function (response) {
            $scope.card = response;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        }
    );

    $scope.editRequested = false;
    $scope.deleteRequested = false;

    $scope.requestDelete = function () {
        $scope.deleteRequested = true;
    };

    $scope.requestEdit = function () {
        $scope.editRequested = true;
        enableEditing();
    };

    $scope.cancelDeleteRequest = function () {
        $scope.deleteRequested = false;
    };

    $scope.deleteConcept = function () {
        cardFactory.remove({id:$stateParams.id});
        $state.go('app.conceptlist');
        $scope.deleteRequested = false;
    };

    $scope.cancelEditRequest = function () {
        $scope.editRequested = false;
        disableEditing();
    };

    $scope.saveConcept = function () {


        cardFactory.update({id:$stateParams.id}, $scope.card);
        disableEditing();
        //$state.go($state.current, {}, {reload: true});
        /*
        cardFactory.update({ id: $stateParams.id })
        .$promise.then(
            function (response) {
                $scope.card = response;
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
            }
        );
        */
        $scope.editRequested = false;
    };
}])
;
