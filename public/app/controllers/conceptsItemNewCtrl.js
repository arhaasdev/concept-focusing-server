'use strict';

angular.module('conceptFocusToolApp')

.controller('ConceptsItemNewCtrl', ['$scope', '$state', 'cardFactory', function($scope, $state, cardFactory) {

    $scope.editRequested = true;
    $scope.card = {
        name: "new concept",
        definition: "new concept definition",
        relatedConcept1: "related concept 1",
        relation1Description: "",
        relatedConcept2: "",
        relation2Description: "",
        relatedConcept3: "",
        relation3Description: "",
        relatedConcept4: "",
        relation4Description: ""
    };







    $scope.continueEditing = function() {
        $scope.editRequested = true;
    };

    $scope.quitEditing = function() {
        $state.go('app.cardview', { id:$stateParams.id });
    };

    $scope.saveConcept = function() {
        cardFactory.save($scope.card);
        $state.go('app.conceptlist');
    }
}]);
