var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create schema
var Card = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        definition: {
            type: String,
            default: ""
        },
        relatedConcept1: {
            type: String,
            default: ""
        },
        relation1Description: {
            type: String,
            default: ""
        },
        relatedConcept2: {
            type: String,
            default: ""
        },
        relation2Description: {
            type: String,
            default: ""
        },
        relatedConcept3: {
            type: String,
            default: ""
        },
        relation3Description: {
            type: String,
            default: ""
        },
        relatedConcept4: {
            type: String,
            default: ""
        },
        relation4Description: {
            type: String,
            default: ""
        }
    },
    {
        timestamps: true
    }
);

// create schema
var CardList = new Schema(
    {
        ownedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
            unique: true
        },
        cards: [Card]
    },
    {
        timestamps: true
    }
);

// create model and make available to this server application
module.exports = mongoose.model('CardList', CardList);
//module.exports = mongoose.model('Card', Card); // QUESTIONABLE
