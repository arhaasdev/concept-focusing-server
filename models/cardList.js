var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create schema
var CardList = new Schema(
    {
        ownedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
            unique: true
        },
        cards: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Card',
            unique: true
        }]
    },
    {
        timestamps: true
    }
);

// create model and make available to this server application
module.exports = mongoose.model('CardList', CardList);
