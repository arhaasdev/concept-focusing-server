var express = require('express');
var cardsRouter = express.Router();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Card = require('../models/card');
var Verify = require('./verify');

cardsRouter.use(bodyParser.json());


cardsRouter.route('/')
.get(Verify.verifyOrdinaryUser, function(req, res, next) {
    console.log(req.decoded._id);
    Card.find({ ownedBy: req.decoded._id })  // get all of the user's cards
            .exec(function(err, myCards) {
                if (err) throw err;
                res.json(myCards);
            });
})

.post(Verify.verifyOrdinaryUser, function(req, res, next) {
      req.body.ownedBy = req.decoded._id;
      new Card(req.body).save(function (err, myCard) { // add a new card
          if (err) throw err;
          res.json(myCard);
      });
})



cardsRouter.route('/:objectId')
    .get(Verify.verifyOrdinaryUser, function(req, res, next) { // get a card
        Card.findById(req.params.objectId, function(err, card) {
            if (err) throw err;
            res.json(card);
        });
    })

    .put(Verify.verifyOrdinaryUser, function(req, res, next) { // update a card
        Card.findByIdAndUpdate(req.params.objectId,
            { $set: req.body },
            { new: true },
            function(err, card) {
                if (err) throw err;
                res.json(card);
        });
    })

    .delete(Verify.verifyOrdinaryUser, function(req, res, next) { // delete a card
        Card.findByIdAndRemove(req.params.objectId, function(err, resp) {
            if (err) throw err;
            res.json(resp);
        });
    });


module.exports = cardsRouter;
