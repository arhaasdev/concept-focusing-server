var express = require('express');
var cardListRouter = express.Router();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var CardList = require('../models/cardList');
var Verify = require('./verify');

cardListRouter.use(bodyParser.json());

 console.log("TEST");

cardListRouter.route('/')
.get(Verify.verifyOrdinaryUser, function(req, res, next) {
     console.log("derf rules!");
  userId = req.decoded._id;
    CardList.findOne({ ownedBy: userId })  // get user's cardList object
            .exec(function(err, myCardList) {
                if (err) throw err;
                res.json(myCardList);
            });
})

.post(Verify.verifyOrdinaryUser, function(req, res, next) {
  CardList.findOne({ ownedBy: req.decoded._id }, function (err, myCardList) {  // lets see if this user has a myCardList
      if (err) throw err;
      if (myCardList) { // if there already is a myCardList document
          myCardList.cards.push(req.body);  // push it
          myCardList.save(function (err, myCardList) {  // push it good
              if (err) throw err;  // and we don't need no stinkin errors
                  res.json(myCardList);   // but let's let requester know what we did
              });
      } else {  // if a CardList document doesn't exist for this user
          CardList.create({ // make one
              ownedBy: req.decoded._id,  // for this user
                                                //create a card
              cards: [req.body]    // and add the card too
          }, function (err, myCardList) { // plus the unnecessary error
              if (err) throw err;     // handling
              res.json(myCardList);  // and finally, let's let requester know what we did
          });
      }
    });
})




var promise = Comment.find({ "author": userId }).select("post").exec();
promise.then(function (comments) {
    var postIds = comments.map(function (c) {
        return c.post;
    });
    return Post.find({ "_id": { "$in": postIds }).exec();
}).then(function (posts) {
    // do something with the posts here
    console.log(posts);

}).then(null, function (err) {
    // handle error here
});

var promise = CardList.findOne({ "ownedBy": req.decoded._id }).select("post").exec();
promise.then(function (comments) {
    var postIds = comments.map(function (c) {
        return c.post;
    });
    return Post.find({ "_id": { "$in": postIds }).exec();
}).then(function (posts) {
    // do something with the posts here
    console.log(posts);

}).then(null, function (err) {
    // handle error here
});









cardListRouter.route('/:objectId')
    .get(Verify.verifyOrdinaryUser, function(req, res, next) {
        console.log("req.params.objectId ", req.params.objectId);
        console.log("req.body ", req.body);
        CardList.findOne({ownedBy:req.decoded._id, cards._id:req.params.objectId}, function(err, card) {
            if (err) throw err;
            res.json(card);
        });
    })






/* PUT LOGIC
        CardList.findByIdAndUpdate(req.params.objectId, {
//        Card.findByIdAndUpdate(req.params.objectId, {       //QUESTIONABLE
            $set: req.body
        }, {
            new: true
        }, function(err, card) {
            console.log("req.params.objectId ", req.params.objectId);   //QUESTIONABLE
            console.log("req.body ", req.body);                         //QUESTIONABLE
            if (err) throw err;
            res.json(card);
        });
    })


    .delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req, res, next) {
        CardList.remove({}, function(err, resp) {
            if (err) throw err;
            res.json(resp);
        });
    });




    .delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req, res, next) {
        CardList.findByIdAndRemove(req.params.leaderId, function(err, resp) {
            if (err) throw err;
            res.json(resp);
        });
    });
*/
module.exports = cardListRouter;
